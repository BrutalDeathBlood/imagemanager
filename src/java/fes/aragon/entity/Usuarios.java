/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author eloy
 */
@Entity
@Table(name = "Usuarios")
@NamedQueries({
     @NamedQuery(name = "Usuarios.acceso",query = "SELECT u FROM Usuarios u WHERE u.usuario=?1 and u.password=?2"),
    @NamedQuery(name = "Usuarios.findAll", query = "SELECT u FROM Usuarios u")
   })
public class Usuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "Nombre",nullable = false)
    private String nombre;
    @Column(name = "Apellido_Materno",nullable = false)
    private String apm;
    @Column(name = "Apellido_Paterno",nullable = false)
    private String app;
    @Column(name = "Edad",nullable = false)
    private int edad;
    @Column(name = "Correo_Electronico",nullable = false)
    private String correo;
    @Column(name = "Login",nullable = false)
    private String usuario;
    @Column(name = "Password",nullable = false)
    private String password;
    @Column(name = "Rol_Usuario",nullable = false)
    private String rol;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuariosid")
    private List<Imagenes> imagenesList;
    

    public Usuarios() {
    }

    public Usuarios(Integer id) {
        this.id = id;
    }

    public Usuarios(Integer id, String nombre, String apellidoMaterno, String apellidoPaterno, int edad, String correo, String usuario, String password, String rol) {
        this.id = id;
        this.nombre = nombre;
        this.apm = apellidoMaterno;
        this.app = apellidoPaterno;
        this.edad = edad;
        this.usuario = usuario;
        this.password = password;
        this.rol = rol;
        this.correo = correo;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApm() {
        return apm;
    }

    public void setApm(String apm) {
        this.apm = apm;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
    
    public List<Imagenes> getImagenesList() {
        return imagenesList;
    }

    public void setImagenesList(List<Imagenes> imagenesList) {
        this.imagenesList = imagenesList;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuarios)) {
            return false;
        }
        Usuarios other = (Usuarios) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fes.aragon.entity.Usuarios[ id=" + id + " ]";
    }
    
}
