/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author eloy
 */
@Entity
@Table(name = "Imagenes")
@NamedQueries({
    @NamedQuery(name = "Imagenes.obtenerImagenesDeUsuario", query = "SELECT i FROM Imagenes i WHERE i.usuariosid=?1"),
    @NamedQuery(name = "Imagenes.findAll", query = "SELECT i FROM Imagenes i")})
public class Imagenes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "nombre",nullable = false)
    private String nombre;
    @Column(name = "extension",nullable = false)
    private String extension;
    @JoinColumn(name = "Usuarios_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuarios usuariosid;
    @Transient
    private StreamedContent imageInServer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Usuarios getUsuariosid() {
        return usuariosid;
    }

    public void setUsuariosid(Usuarios usuariosid) {
        this.usuariosid = usuariosid;
    }

    public StreamedContent getImageInServer() {
        return imageInServer;
    }

    public void setImageInServer(StreamedContent imageInServer) {
        this.imageInServer = imageInServer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Imagenes)) {
            return false;
        }
        Imagenes other = (Imagenes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fes.aragon.entity.Imagenes[ id=" + id + " ]";
    }
    
}
