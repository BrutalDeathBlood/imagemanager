/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.ejb;

import fes.aragon.entity.Imagenes;
import fes.aragon.entity.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author eloy
 */
@Stateless
public class ImagenesFacade extends AbstractFacade<Imagenes> implements ImagenesFacadeLocal {

    @PersistenceContext(unitName = "ProyectoEloyPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ImagenesFacade() {
        super(Imagenes.class);
    }

    @Override
    public List<Imagenes> obtenerImagenesDeUsuario(Usuarios usuario) {
        List<Imagenes> imagenesUsuario=null;
        Imagenes imagen=new Imagenes();
        imagen.setUsuariosid(usuario);
        try {
            Query query=em.createNamedQuery("Imagenes.obtenerImagenesDeUsuario");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1,imagen.getUsuariosid());
            imagenesUsuario=query.getResultList();
        } catch (Exception e) {
            throw e;
        }
        return imagenesUsuario;
    }
    
}
