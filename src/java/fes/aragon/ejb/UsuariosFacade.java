/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.ejb;

import fes.aragon.entity.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author eloy
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> implements UsuariosFacadeLocal {

    @PersistenceContext(unitName = "ProyectoEloyPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }

    @Override
    public Usuarios LogIn(Usuarios usuario) {
        Usuarios usuarioFinal=null;
        try {
            Query query=em.createNamedQuery("Usuarios.acceso");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1,usuario.getUsuario());
            query.setParameter(2,usuario.getPassword());
            List<Usuarios> usuarios=query.getResultList();
            if(!usuarios.isEmpty()){
                for(int i = 0; i < usuarios.size(); i++){
                    usuarioFinal=usuarios.get(i);
                    break;
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return usuarioFinal;
    }
    
}
