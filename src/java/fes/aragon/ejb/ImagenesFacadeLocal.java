/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.ejb;

import fes.aragon.entity.Imagenes;
import fes.aragon.entity.Usuarios;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author eloy
 */
@Local
public interface ImagenesFacadeLocal {

    void create(Imagenes imagenes);

    void edit(Imagenes imagenes);

    void remove(Imagenes imagenes);

    Imagenes find(Object id);

    List<Imagenes> findAll();
    
    List<Imagenes> obtenerImagenesDeUsuario(Usuarios usuario);

    List<Imagenes> findRange(int[] range);

    int count();
    
}
