/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.bean;

import fes.aragon.ejb.ImagenesFacadeLocal;
import fes.aragon.entity.Imagenes;
import fes.aragon.entity.Usuarios;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author eloy
 */
@ManagedBean
@RequestScoped
public class UploadDownloadBean implements Serializable{
    
    @EJB
    private ImagenesFacadeLocal servicioEJB;
    private Imagenes images;  
    private StreamedContent image;
    private StreamedContent download;
    private Usuarios user;
    private final String basePath = "/home/alan/payara41/glassfish/domains/payaradomain/Archivos/";
    private String path = "";

    
    /**
     * Creates a new instance of UploadDownloadBean
     */
    public UploadDownloadBean() {
    }
    
    @PostConstruct
    public void initVariables(){
        try {
            user=(Usuarios) FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .get("credencial");
            images = new Imagenes();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("alertas-img", new FacesMessage("Error, contacte al Administrador."));
        }
    }
    
    public void uploadImage(FileUploadEvent event){
        generateUserPath();
        BufferedImage bufimg=null;
        String fileName;
        try {
            createImage(event.getFile().getFileName(),event.getFile().getInputstream());
            fileName = path+"/"+event.getFile().getFileName();
            System.out.println(event.getFile().getFileName());
            String ArchName = event.getFile().getFileName();
            String extension = ArchName.substring(ArchName.lastIndexOf(".") + 1);
            String name = ArchName.substring(0,ArchName.lastIndexOf("."));
            File arch=new File(fileName);
            if(arch.exists()){
                bufimg=ImageIO.read(arch);
                images.setNombre(name);
                images.setExtension(extension);
                images.setUsuariosid(user);
                servicioEJB.create(images);
                ImageIO.write(bufimg,extension,new File(fileName));
                image=new DefaultStreamedContent(new FileInputStream(fileName),"image/"+extension);
                FacesContext.getCurrentInstance().addMessage("alertas-img", new FacesMessage("Datos Almacenados."));
            }
        } catch (IOException e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage("alertas-img", new FacesMessage("Error, contacte al Administrador."));

        }
    }
    
    public void createImage(String nombre,InputStream entrada){
        try{
            OutputStream salida=new FileOutputStream(new File(path+"/"+nombre));
            int read=0;
            byte[] bytes=new byte[1024];
            while((read=entrada.read(bytes))!=-1){
                salida.write(bytes, 0, read);
            }
            entrada.close();
            salida.flush();
            salida.close();
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    
    public void generateUserPath(){
        File directory = new File(basePath+user.getUsuario());
        // if the directory does not exist, create it
        if (!directory.exists()) {
            FacesContext.getCurrentInstance().addMessage("alertas-img", new FacesMessage("Creando el Directorio "+user.getUsuario()));
            boolean result = false;

            try{
                directory.mkdir();
                result = true;
            } 
            catch(SecurityException se){
                //handle it
            }        
            if(result) {    
                FacesContext.getCurrentInstance().addMessage("alertas-img", new FacesMessage("Directorio "+user.getUsuario() + " Creado."));
                path = basePath+user.getUsuario();
            }
        }else{
            path = basePath+user.getUsuario();
            FacesContext.getCurrentInstance().addMessage("alertas-img", new FacesMessage("Subiendo imagen al directorio: "+user.getUsuario()));
        }
    }

    public StreamedContent getImage() {
        return image;
    }
    
    
    public StreamedContent getDownload(Imagenes images) {
        FileInputStream input=null;
        try{
            if(user.getRol().equals("Usuario")){
                input=new FileInputStream(basePath+user.getUsuario()+"/"+images.getNombre()+"."+images.getExtension());
                download=new DefaultStreamedContent(input, "image/"+images.getExtension(),images.getNombre()+"."+images.getExtension());
            }else if(user.getRol().equals("Administrador")){
                input=new FileInputStream(basePath+images.getUsuariosid().getUsuario()+"/"+images.getNombre()+"."+images.getExtension());
                download=new DefaultStreamedContent(input, "image/"+images.getExtension(),images.getNombre()+"."+images.getExtension());
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return download;
    }

    public void setDowload(StreamedContent download) {
        this.download = download;
    }
}
