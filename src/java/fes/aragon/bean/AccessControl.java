/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.bean;

import fes.aragon.entity.Usuarios;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

/**
 *
 * @author eloy
 */
@ManagedBean
@ViewScoped
public class AccessControl implements Serializable{
    
    /**
     * Creates a new instance of AccessControl
     */
    public AccessControl() {
    }
    
    public void checkSession(){
        FacesContext contexto=FacesContext.getCurrentInstance();
        try {    
            Usuarios credencials=(Usuarios)contexto.getExternalContext().getSessionMap().get("credencial");
            if(credencials==null){
                contexto.getExternalContext().redirect("./../Principal/Login.jsf");
            }else{
               String pagina=contexto.getViewRoot().getViewId();
               String[] tokens=pagina.split("/");
               if(tokens[1].equals("AccesoUsuario")){
                   if(!credencials.getRol().equals("Usuario")){
                       contexto.getExternalContext().redirect("./../AccesoAdministrador/Principal.jsf");
                   }
               }
               if(tokens[1].equals("AccesoAdministrador")){
                   if(!credencials.getRol().equals("Administrador")){
                       contexto.getExternalContext().redirect("./../AccesoUsuario/Principal.jsf");
                   }
               }
            }
        } catch (Exception e) {
        }
    }
    public void sessionDestroy(){
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().invalidateSession();
            contexto.getExternalContext().redirect("./../Principal/Login.jsf");
        } catch (Exception e) {
        }
    }
    
    public String getUserType(){
        FacesContext contexto=FacesContext.getCurrentInstance();
        Usuarios usuario=(Usuarios)contexto.getExternalContext().getSessionMap().get("credencial");
        return usuario.getRol();
    }
    public int getUserID(){
        FacesContext contexto=FacesContext.getCurrentInstance();
        Usuarios usuario= (Usuarios)contexto.getExternalContext().getSessionMap().get("credencial");
        return usuario.getId();
    }
    
    public String getUserName(){
        FacesContext contexto=FacesContext.getCurrentInstance();
        Usuarios usuario= (Usuarios)contexto.getExternalContext().getSessionMap().get("credencial");
        return usuario.getUsuario();
    }
}
