/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.bean;

import fes.aragon.ejb.UsuariosFacadeLocal;
import fes.aragon.entity.Usuarios;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author eloy
 */
@ManagedBean
@RequestScoped
public class RegisterBean implements Serializable{
    @EJB
    private UsuariosFacadeLocal servicioEJB;
    private Usuarios usuario;
    /**
     * Creates a new instance of RegisterBean
     */
    public RegisterBean() {
    }
    @PostConstruct()
    public void initVariables(){
        usuario = new Usuarios();
    }

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }
    
    public void registro(){
        try {
            usuario.setPassword(DigestUtils.sha256Hex(usuario.getPassword()));
            usuario.setRol("Usuario");
            servicioEJB.create(usuario);
            FacesContext.getCurrentInstance().addMessage("alertas-registro", new
            FacesMessage("Datos Almacenados."));
            usuario = new Usuarios();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("alertas-registro", new
            FacesMessage("Error, contacte al Administrador."));
        }
    }
    
    public String acceso(){
        Usuarios temporal=null;
        String cadena=null;
        try {
            usuario.setPassword(DigestUtils.sha256Hex(usuario.getPassword()));
            temporal=servicioEJB.LogIn(usuario);
            if(temporal!=null){
                FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .put("credencial", temporal);
                if(temporal.getRol().equals("Usuario")){
                    cadena="/AccesoUsuario/Principal?faces-redirect=true";
                }else{
                    cadena="/AccesoAdministrador/Principal?faces-redirect=true";
                }
                
            }else{
                FacesContext.getCurrentInstance().addMessage("alertas-registro",
                        new FacesMessage("Usuario o Password Erroneos."));
                usuario=new Usuarios();
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("alertas-registro",
                        new FacesMessage("Error, contacte al Administrador."));
        }
        return cadena;
    }
}
