/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.bean;

import fes.aragon.ejb.UsuariosFacadeLocal;
import fes.aragon.entity.Usuarios;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author eloy
 */
@ManagedBean
@ViewScoped
public class UsersBean implements Serializable{
    @EJB
    private UsuariosFacadeLocal servicioEJB;
    private Usuarios user;
    private Usuarios foundUser;
    private String currentPass;
    private List<Usuarios>usersList;
    private String basePath = "/home/alan/payara41/glassfish/domains/payaradomain/Archivos/";
    
    /**
     * Creates a new instance of UsersBean
     */
    public UsersBean() {
    }
    
    @PostConstruct
    public void initVariables(){
        try {
            user=(Usuarios) FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .get("credencial");
            foundUser=(Usuarios)servicioEJB.find(user.getId());
            currentPass = user.getPassword();
            usersList=servicioEJB.findAll();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("alertas-editar", new FacesMessage("Error, contacte al Administrador."));
        }
    }
    
    public void editProfile(){
        try {
            if(!currentPass.equals(user.getPassword()) && !user.getPassword().equals("")){
                String aux=DigestUtils.sha256Hex(user.getPassword());
                user.setPassword(aux);
            }else if(user.getPassword().equals("")){
                user.setPassword(currentPass);
            }
            if(Verificar() == false){
                FacesContext.getCurrentInstance().addMessage("alertas-editar", new FacesMessage("Ningun Cambio Detectado."));
            }else{
                servicioEJB.edit(user);
                FacesContext.getCurrentInstance()
                            .getExternalContext()
                            .getSessionMap().clear();
                FacesContext.getCurrentInstance()
                            .getExternalContext()
                            .getSessionMap().put("credencial",user);
                FacesContext.getCurrentInstance().addMessage("alertas-editar", new FacesMessage("Perfil editado correctamente."));
                user=(Usuarios) FacesContext.getCurrentInstance()
                            .getExternalContext()
                            .getSessionMap()
                            .get("credencial");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("alertas-editar", new FacesMessage("Error, contacte al Administrador."));
        }
    }
    public void deleteUser(Usuarios userTemp){
        try {
            servicioEJB.remove(userTemp);
            String path = basePath+userTemp.getUsuario();
            File file = new File(path);
            if(file.exists()){
                deleteFile(file);
                FacesContext.getCurrentInstance().addMessage("alertas-eliminar", new FacesMessage("Carpeta "+userTemp.getUsuario()+" Eliminada."));
            }
            FacesContext.getCurrentInstance().addMessage("alertas-eliminar", new FacesMessage("Usuario Eliminado"));
            usersList=servicioEJB.findAll();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("alertas-eliminar", new FacesMessage("Error, contacte al Administrador."));
        }
    }
    
    public boolean isAdmin(Usuarios temp){
        if(temp.getRol().equals("Administrador")){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean isUser(Usuarios temp){
        if(temp.getRol().equals("Usuario")){
            return true;
        }else{
            return false;
        }
    }
    
    
    public void deleteFile(File element) {
        if (element.isDirectory()) {
            for (File sub : element.listFiles()) {
                deleteFile(sub);
            }
        }
        element.delete();
    }
    
    public boolean Verificar(){
        if(!user.getNombre().equals(foundUser.getNombre()) 
                || !user.getApm().equals(foundUser.getApm())
                || !user.getApp().equals(foundUser.getApp())
                || user.getEdad() != foundUser.getEdad()
                || !user.getCorreo().equals(foundUser.getCorreo())
                || !user.getUsuario().equals(foundUser.getUsuario())
                || !user.getPassword().equals(user.getPassword())){
            return true;
        }else{
            return false;
        }
    }

    public Usuarios getUser() {
        return user;
    }

    public void setUser(Usuarios user) {
        this.user = user;
    }

    public Usuarios getFoundUser() {
        return foundUser;
    }

    public void setFoundUser(Usuarios foundUser) {
        this.foundUser = foundUser;
    }

    public String getCurrentPass() {
        return currentPass;
    }

    public void setCurrentPass(String currentPass) {
        this.currentPass = currentPass;
    }

    public List<Usuarios> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Usuarios> usersList) {
        this.usersList = usersList;
    }
    
}
