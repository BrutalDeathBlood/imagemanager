/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fes.aragon.bean;

import fes.aragon.ejb.ImagenesFacadeLocal;
import fes.aragon.entity.Imagenes;
import fes.aragon.entity.Usuarios;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author eloy
 */
@ManagedBean
@ViewScoped
public class ImagesBean implements Serializable{
    
    @EJB
    private ImagenesFacadeLocal servicioEJB;
    private Imagenes image;
    private List<Imagenes>imageList;
    private String path = "";
    private Usuarios user;
    private String basePath = "/home/alan/payara41/glassfish/domains/payaradomain/Archivos/";

    
    /**
     * Creates a new instance of ImagesBean
     */
    public ImagesBean() {
    }
    
    @PostConstruct
    public void initVariables(){
        image = new Imagenes();
        try {
            user=(Usuarios) FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .get("credencial");
            if(user.getRol().equals("Usuario")){
                path = basePath+user.getUsuario();
                displayImages();
            }else if(user.getRol().equals("Administrador")){
                displayAllImages();
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("alertas-img", new FacesMessage("Error, contacte al Administrador."));
        }
    }

    public Imagenes getImage() {
        return image;
    }

    public void setImage(Imagenes image) {
        this.image = image;
    }

    public List<Imagenes> getImageList() {
        return imageList;
    }

    public void setImageList(List<Imagenes> imageList) {
        this.imageList = imageList;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    public void displayImages(){
        imageList = servicioEJB.obtenerImagenesDeUsuario(user);
        File arch=null;
        for (Imagenes imagenes : imageList) { 
            arch=new File(path+"/"+imagenes.getNombre()+"."+imagenes.getExtension());
            if(arch.exists()){
                try {
                    imagenes.setImageInServer(new DefaultStreamedContent(new FileInputStream(arch),"image/"+imagenes.getExtension()));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ImagesBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    } 
    
    public void displayAllImages(){
        imageList = servicioEJB.findAll();
        File arch=null;
        String slug ="";
        for (Imagenes imagenes : imageList) { 
            slug = imagenes.getUsuariosid().getUsuario();
            path = basePath + slug;
            arch=new File(path+"/"+imagenes.getNombre()+"."+imagenes.getExtension());
            if(arch.exists()){
                try {
                    imagenes.setImageInServer(new DefaultStreamedContent(new FileInputStream(arch),"image/"+imagenes.getExtension()));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ImagesBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void deleteImage(Imagenes image){
        try {
            servicioEJB.remove(image);
            File file = new File(path+"/"+image.getNombre()+"."+image.getExtension());
            if(file.exists()){
                deleteFile(file);
            }
            FacesContext.getCurrentInstance().addMessage("alertas-img", new FacesMessage("Imagen "+image.getNombre()+"."+image.getExtension()+" Eliminada."));
            if(user.getRol().equals("Usuario")){
                displayImages();
            }
            image=new Imagenes();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("mensaje", new FacesMessage("Error en la BD consultar al administrador"));
        }
    }
    
    public void deleteFile(File element) {
        if (element.isDirectory()) {
            for (File sub : element.listFiles()) {
                deleteFile(sub);
            }
        }
        element.delete();
    }
}
